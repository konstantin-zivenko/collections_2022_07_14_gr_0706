# ChainMap

from collections import ChainMap

cmd_proxy = {"output_file": "my_local_file"}
local_proxy = {"proxy": "proxy.local.com", "output_file": "my_standart_output"}
global_proxy = {"proxy": "proxy.global.com", "api_rout": "global_api_rout"}

config = ChainMap(cmd_proxy, local_proxy, global_proxy)

print(config["proxy"])

print(dict(config))

print(config.get("ttt"))