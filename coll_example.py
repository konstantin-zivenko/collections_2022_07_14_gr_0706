# namedtuple

from collections import namedtuple

employees = namedtuple("employees", "name age position salary")

employees_1 = employees("Nadia Stepanova", 35, "sales manager", 50000)
employees_2 = employees("Olga Shevchenko", 38, "python developer", 90000)

def custom_divmod(x, y):
    DivMod = namedtuple("DivMod", "quotient remainder")
    return DivMod(*divmod(x, y))

result = custom_divmod(15, 6)
print(result)
print(result.quotient)
print(result.remainder)

# deque
# FIFO LIFO
# O(n) O(n) O(1)

from collections import deque

ticket_queue = deque()

ticket_queue.append("Maria")
ticket_queue.append("Igor")
ticket_queue.append("Stepan")

ticket_queue.appendleft("Fedor")







