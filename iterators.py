"""
Реализуйте цикл, который будет перебирать все значения итерабельного объекта iterable.
"""
# for i in <iterable>:
#     print(i)
# 1) itr = iter(iterable) -> itr = iterable.__iter__()
# 2) i = next(itr) -> i = itr.__next__()
# 3) raise StopIteration

class MyClass(int):
    def __init__(self, n):
        self.value = n
        super().__init__()

    def __iter__(self):
        self.intermedia_res = self.value
        return self

    def __next__(self):
        res = divmod(self.intermedia_res, 10)
        if res[1]:
            self.intermedia_res = res[0]
            return res[1]
        raise  StopIteration


aaa = MyClass(2345)

for i in aaa:
    print(i)

